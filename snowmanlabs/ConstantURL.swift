//
//  ConstantURL.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation


var BASE:String = "http://api.themoviedb.org/"
var API:String = "/3"

var BASE_URL_API:String = BASE + API
let API_KEY:String = "api_key=5974484d1cdc7474135e04c6c8902486"


/************************/
// MARK: - MOVIES
/************************/

func POPULAR_URL(page:String) -> String {
    return BASE_URL_API + (NSString(format:"/movie/popular?%@&page=%@", API_KEY, page) as String)
}

func RATED_URL(page:String) -> String {
    return BASE_URL_API + (NSString(format:"/movie/top_rated?%@&page=%@", API_KEY, page) as String)
}
