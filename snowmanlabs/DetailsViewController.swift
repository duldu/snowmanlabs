//
//  DetailsViewController.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 11/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import UIKit
import Foundation

class DetailsViewController: UIViewController
{
    
    var movie:MovieVO?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var overviewLabel: UILabel!
    // - MARK: VIEW FLOW
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if let mov = movie {
            
            self.imageView.imageFromServerURL(urlString: "https://image.tmdb.org/t/p/w500/"+mov.backdrop_path)
            
            self.titleLabel.text = mov.original_title
            
            self.overviewLabel.text = mov.overview
            
        }
        
       
            // Fallback on earlier versions
            self.preferredContentSize.height = 200
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func closeViewController(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}
