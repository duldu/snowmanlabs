//
//  ResponseVO.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import EVReflection

class ResponseVO:EVObject
{
    dynamic var page = 0
    dynamic var results: [MovieVO]!
    dynamic var total_results = 0
    dynamic var total_pages = 0
}
