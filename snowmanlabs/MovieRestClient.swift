//
//  MovieRestClient.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation

import Alamofire

enum MovieRestClient {
    
    case Popular(page:String)
    
    case Rated(page:String)
}

extension MovieRestClient: RestEnumProtocol
{
    var path: String {
        switch self {
        case .Popular(let page):
            return POPULAR_URL(page: page)
        case .Rated(let page):
            return RATED_URL(page: page)
        }
    }
    
    var method : HTTPMethod {
        switch self {
        case .Popular:
            return .get
        case .Rated:
            return .get
        }
    }
    
    var parameters: [String:AnyObject]? {
         switch self {
        case .Popular(_):
            return nil
        case .Rated(_):
            return nil
        }
    }
}
