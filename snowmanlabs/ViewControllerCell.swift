//
//  ViewControllerCell.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 09/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import UIKit

protocol MovieViewCellDelegate {
    func onFavoriteMovie(movie:MovieVO)
    func onSelectMovie(movie:MovieVO)
}

class MovieViewCell: CardViewCollectionView {
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var favoriteButton: UIButton!
    
    @IBOutlet weak var selectMovieButton: UIButton!
    
    var delegate:MovieViewCellDelegate?
    
    private var movieVO: MovieVO?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func configureView(movie:MovieVO)
    {
        self.imageView.isHidden = true
        
        if let mov = movie.poster_path {
            
            self.movieVO = movie
            
            self.imageView.imageFromServerURL(urlString: "https://image.tmdb.org/t/p/w500/"+mov)
            
            self.setFavoriteIcon()
            
            self.imageView.isHidden = false
        }
        
    }
    
    func setFavoriteIcon()
    {
        if let movie = self.movieVO {
            self.favoriteButton.setImage(movie.favorite == 0 ? FavoriteTypeEnum.No_Favorite.icon : FavoriteTypeEnum.Favorite.icon, for: .normal)
        }
    }
    @IBAction func onSelectMovie(_ sender: Any) {
        
        
        if let deleg = delegate {
            
            if let movie = self.movieVO {
                
                deleg.onSelectMovie(movie:movie)
                
                
                
            }
            
        }
        
    }
    
    @IBAction func onActionFavoriteMovie(_ sender: Any) {
        
        if let deleg = delegate {
            
            if let movie = self.movieVO {
                
                deleg.onFavoriteMovie(movie:movie)
                
                self.setFavoriteIcon()
                
            }
            
        }
        
    }
}
