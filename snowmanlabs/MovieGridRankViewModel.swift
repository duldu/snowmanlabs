//
//  MovieGridRankViewModel.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 07/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import RealmSwift

class MovieGridRankViewModel:MovieGridViewModelBase {
    
    
    // - MARK: LOAD MOVIES
    
    override func loadMovies(forceToServer:Bool=false)
    {
    
        BaseRestClient.sharedInstance.requestRest(rest: MovieRestClient.Rated(page: String(self.movieGridModel.currentPage)), showAlert: true, sucess:{ (response) -> Void in
            
            let responseVO:ResponseVO = ResponseVO(json: response as? String)
            
            print(" ====== server ====== ")
            
            let _movies = List<MovieVO>()
            
            for movie:MovieVO in responseVO.results {
                _movies.append(movie)
            }
            
            
            //save realm
            
            self.realm.saveMovieModel(movieModel: self.movieGridModel,
                                      loadedPage: self.movieGridModel.currentPage,
                                      totalpages: responseVO.total_pages,
                                      totalresults: responseVO.total_results,
                                      type: GridTypeEnum.Ranking.rawValue,
                                      movies: _movies)
            
            self.loadOfflineData()
            
        }, failure: { (response) -> Void in
            print(response)
        })
        
    }
    
    
    internal override func loadOfflineData()
    {
        let predicate = NSPredicate(format: "type = %d",  GridTypeEnum.Ranking.rawValue)
        
        if let movie = realm.getObjects(type: MovieGridModel.self)?.filter(predicate){
            print(" ===== offline  ===== : RANK ")
        
            // load model view from data
            if (movie.first != nil &&  self.movieGridModel.totalpages == 0)
            {
                let movieGridModel:MovieGridModel = movie.first as! MovieGridModel
                
                self.movieGridModel.currentPage = movieGridModel.currentPage
                self.movieGridModel.loadedPage = movieGridModel.loadedPage
                self.movieGridModel.totalpages = movieGridModel.totalpages
                self.movieGridModel.totalresults = movieGridModel.totalresults
                self.movieGridModel.type = movieGridModel.type
                self.movieGridModel.movies = movieGridModel.movies
                
            }
            
            
            self.delegate?.onLoadedMovies(movies:movie.first)
            
        }
        
    }
    
    // - MARK: FAVORITE MY MOVIE
    
    override func selectedFavorite(movie:MovieVO){
        
        if (self.movieGridModel.movies.count > 0)
        {
            
            let predicateID = NSPredicate(format: "id = %d",  movie.id)
            
            self.realm.saveFavorite(movieModel: self.movieGridModel, predicate: predicateID, favorite: movie.favorite == 0 ? 1 : 0)
            
        }
       
        
    }
    
}
