//
//  ViewController.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource , MovieGridViewModelDelegate, MovieViewCellDelegate {
    
    
    // - MARK: OUTLETS
    
    @IBOutlet weak var gridTypeSegmentControl: UISegmentedControl!
    
    @IBOutlet weak var gridMovieCollectionView: CustomViewCollection!
    
    // - MARK: VARS
    
    private var movieGridPopModel:MovieGridModel = MovieGridModel(type: GridTypeEnum.Pop.rawValue)
    private var movieGridRankModel:MovieGridModel = MovieGridModel(type: GridTypeEnum.Ranking.rawValue)
    private var movieGridFavoriteModel:MovieGridModel = MovieGridModel(type: GridTypeEnum.Favorite.rawValue)
    
    private var moviesGridViewModels:[MovieGridViewModelProtocol] = [MovieGridViewModelProtocol]()
    
    let realm:RealmManager = RealmManager()
    
    var movies:List<MovieVO>?
    
    var positionGrid:[CGFloat] = [0.0, 0.0, 0.0]
    
    // - MARK: CONSTANTS
    
    private var cellWidth:Int  = 160
    private var cellCount:Int = 1
    private var cellSpacing:Int = 5
    private var backgroundColor:UIColor = UIColor.init(hexString: "#D0E8ED")
    
    // securty watcher

    var isLoading:Bool = false
    
    var countMoviesBySegment: [Int] = [0, 0, 0]

    // - MARK: VIEW FLOW
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        Utils.sharedInstance.showLoader()
        // Create view model
        
        self.movieGridFavoriteModel.type = GridTypeEnum.Favorite.rawValue
        
        let movieGridPopViewModel = MovieGridPopViewModel(movieGrid: movieGridPopModel, realm: self.realm)
        let movieGridRankViewModel = MovieGridRankViewModel(movieGrid: movieGridRankModel, realm: self.realm)
        let movieGridFavoriteViewModel = MovieGridFavoriteViewModel(movieGrid: movieGridFavoriteModel, realm: self.realm)
        
        movieGridPopViewModel.delegate = self
        movieGridRankViewModel.delegate = self
        movieGridFavoriteViewModel.delegate = self
        
        // - MARK: LOAD OFFLINE DATA
        movieGridPopViewModel.loadOfflineData()
        movieGridRankViewModel.loadOfflineData()
        movieGridFavoriteViewModel.loadOfflineData()
        
        self.moviesGridViewModels = [movieGridPopViewModel, movieGridRankViewModel, movieGridFavoriteViewModel]
        
        self.gridMovieCollectionView.delegate = self
        self.gridMovieCollectionView.dataSource = self
        
        self.setupSegmentControl()
        self.setupNavigationBar()
        
        self.view.backgroundColor = backgroundColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // - MARK: SETUP`s
    
    private func setupNavigationBar()
    {
        self.navigationController?.navigationBar.barTintColor = backgroundColor
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController!.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        //self.navigationController?.navigationItem.title = "Filmes"
        self.navigationItem.title = "Filmes"
        
     
    }
    
    private func setupSegmentControl()
    {
        let segmentsNames:[String] = [GridTypeEnum.Pop.name, GridTypeEnum.Ranking.name, GridTypeEnum.Favorite.name]
        
        gridTypeSegmentControl.replaceSegments(segments: segmentsNames)
        
        gridTypeSegmentControl.addTarget(self, action: #selector(actionSegmentControl), for: .valueChanged)
        
        gridTypeSegmentControl.selectedSegmentIndex = 0
        
        self.actionSegmentControl(segment: self.gridTypeSegmentControl!)
        
        gridTypeSegmentControl.backgroundColor = backgroundColor
        
    }
    
    // - MARK: COLLECTION VIEW
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
            return self.movies?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieViewCell", for: indexPath as IndexPath) as! MovieViewCell
        
        cell.backgroundColor = UIColor.white
        cell.delegate = self
        
        if let movie = self.movies?[indexPath.row] {
            cell.configureView(movie: movie)
        }
        
        return cell;
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 150.0, height: 220.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 10, 0, 10)
    }
    

    // - MARK: SCROLL VIEW LISTENER
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        
        if GridTypeEnum(rawValue: gridTypeSegmentControl.selectedSegmentIndex) != GridTypeEnum.Favorite{
            
            let offsetY = scrollView.contentOffset.y
            let contentHeight = scrollView.contentSize.height
            
            positionGrid[gridTypeSegmentControl.selectedSegmentIndex] = offsetY
            
            
            if offsetY > contentHeight - scrollView.frame.size.height && self.isLoading == false && countMoviesBySegment[gridTypeSegmentControl.selectedSegmentIndex] == self.gridMovieCollectionView.numberOfItems(inSection: 0) {
                
                print("loading")
                self.isLoading = true
                if let movieGridViewModel = self.getMovieGridViewModelBySegment(index: self.gridTypeSegmentControl.selectedSegmentIndex){
                    movieGridViewModel.nextPage(increment: true)
                }
            }
        }
    }
    
    
    // - MARK: ACTIONS
    
    @objc func actionSegmentControl(segment: UISegmentedControl)
    {
       
        var point:CGPoint = gridMovieCollectionView.contentOffset
        
        point.y = positionGrid[gridTypeSegmentControl.selectedSegmentIndex]
        
        gridMovieCollectionView.contentOffset = point
        
        if let movieGridViewModel = self.getMovieGridViewModelBySegment(index: segment.selectedSegmentIndex)
        {
            movieGridViewModel.nextPage(increment: false)
        }
        
    }
    
    // - MARK: UTILS
    
    func getMovieGridViewModelBySegment(index: Int) -> MovieGridViewModelProtocol?
    {
        guard index <= self.moviesGridViewModels.count else {
            return nil
        }
        
        return self.moviesGridViewModels[index]
        
    }
    
    func setMovieFavoriteInViewModels(movie:MovieVO)
    {
        for movieGridViewModel in self.moviesGridViewModels
        {
            movieGridViewModel.selectedFavorite(movie: movie)
        }
    }
    
    
    // - MARK: DELEGATES FROM MODEL VIEW
    
    func onLoadedMovies(movies:Object?){
        
        self.isLoading = false
        
        Utils.sharedInstance.hideLoader()
        
        
        self.movies = (movies as? MovieGridModel)?.movies ?? List<MovieVO>()
        
        cellCount = self.movies?.count ?? 1
        
        countMoviesBySegment[gridTypeSegmentControl.selectedSegmentIndex] = self.movies?.count ?? 0
        
        self.gridMovieCollectionView.reloadData()
       
    }
    
    func onFavoriteMovie(movie:MovieVO)
    {
        self.setMovieFavoriteInViewModels(movie: movie)
    }
    
    func onSelectMovie(movie:MovieVO)
    {
        
        let detailController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailsViewController") as! DetailsViewController
        
        detailController.modalPresentationStyle = .pageSheet
        
        detailController.movie = movie
        
        //self.navigationController?.pushViewController(detailController, animated: true)
        
        self.present(detailController, animated: true, completion: nil)
        
    }

}

