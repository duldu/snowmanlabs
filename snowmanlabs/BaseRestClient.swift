//
//  BaseRestClient.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import Alamofire

class BaseRestClient {
    static let sharedInstance: BaseRestClient = {
        let instance = BaseRestClient()
        // setup code
        return instance
    }()
    func request(url: String, method:HTTPMethod,  showAlert:Bool = false, parameters: Dictionary<String, AnyObject>? = nil, sucess: @escaping (AnyObject) -> Void, failure: @escaping (AnyObject) -> Void) {
        
        if showAlert {
            Utils.sharedInstance.showLoader()
        }
        Alamofire.request(url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: headerAuth())
            .responseString { (response) in
                switch response.result {
                case .success:
                    if let value = response.result.value {
                        if showAlert {
                            Utils.sharedInstance.hideLoader()
                        }
                        sucess(value as AnyObject)
                    }
                case .failure(let error):
                    failure(error as AnyObject)
                }
        }
    }
    
    // request by RestProtocol
    func requestRest(rest : RestEnumProtocol, showAlert:Bool = false,  sucess: @escaping (AnyObject) -> Void, failure: @escaping (AnyObject) -> Void) {
        self.request(url: rest.path, method: rest.method, showAlert: showAlert, parameters: rest.parameters, sucess: sucess, failure: failure)
    }
    func headerAuth() -> HTTPHeaders
    {
        return [:]
    }
}
