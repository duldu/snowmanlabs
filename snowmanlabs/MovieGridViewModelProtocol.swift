//
//  MovieGridViewModelProtocol.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 06/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation

protocol MovieGridViewModelProtocol {
    
    var realm:RealmManager { get set }
    
    var movieGridModel:MovieGridModel { get set }
    
    func loadMovies(forceToServer:Bool)
    
    func nextPage(increment:Bool)
    
    func loadOfflineData()
    
    func selectedFavorite(movie:MovieVO)
    
}
