//
//  RealmManager.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 08/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation

import RealmSwift

class RealmManager {
    
    let realm = try! Realm()
    
    func deleteDatabase() {
        try! realm.write({
            realm.deleteAll()
        })
    }
    
    func saveMovieModel(movieModel: MovieGridModel, loadedPage:Int, totalpages: Int, totalresults:Int, type:Int, movies:List<MovieVO>)
    {
        try! realm.write({
            
            movieModel.loadedPage = loadedPage
            movieModel.totalpages = totalpages
            movieModel.totalresults = totalresults
            
            if(movieModel.type == 99)
            {
                movieModel.type = type
            }
            
            
            movieModel.movies.append(objectsIn: movies)
            realm.add(movieModel, update: true)
        })
    }
    
    func saveFavorite(movieModel: MovieGridModel, predicate: NSPredicate, favorite:Int)
    {
        try! realm.write({
            movieModel.movies.filter(predicate).first?.favorite = favorite
            realm.add(movieModel, update: true)
        })
    }
    
    func nextPage(movieModel: MovieGridModel)
    {
        try! realm.write({
            // If update = true, objects that are already in the Realm will be
            // updated instead of added a new.
            movieModel.currentPage = movieModel.currentPage + 1
            realm.add(movieModel, update: true)
        })
    }
    func updateMovies(movieModel: MovieGridModel, movies:List<MovieVO>)
    {
        try! realm.write({
            // If update = true, objects that are already in the Realm will be
            // updated instead of added a new.
            movieModel.movies.append(objectsIn: movies)
            realm.add(movieModel, update: true)
        })
    }

    func saveObjects(obj: Object) {
        try! realm.write({
            // If update = true, objects that are already in the Realm will be
            // updated instead of added a new.
            realm.add(obj, update: true)
        })
    }
    
    func getObjects(type: Object.Type) -> Results<Object>? {
        return realm.objects(type)
    }
}
