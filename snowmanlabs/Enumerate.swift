//
//  Enumerate.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 04/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import UIKit

enum GridTypeEnum: Int {
    case Pop = 0
    case Ranking
    case Favorite
    
    var name:String {
        switch self {
        case .Pop:
            return "Popular"
        case .Ranking:
            return "Top"
        case .Favorite:
            return "Favoritos"
        }
    }
}

enum FavoriteTypeEnum: Int {
    case No_Favorite = 0
    case Favorite
    
    var icon: UIImage
    {
        switch self {
        case .No_Favorite:
            return UIImage(named: "favorite_icon")!
        case .Favorite:
            return UIImage(named:"favorite_fill_icon")!
        }
    }
    
    
}
