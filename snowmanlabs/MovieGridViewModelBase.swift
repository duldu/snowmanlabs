//
//  MovieGridViewModelBase.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 10/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation

class MovieGridViewModelBase:MovieGridViewModelProtocol
{
    
    // - MARK: VARIABLES
    
    var movieGridModel:MovieGridModel
    
    var realm:RealmManager
    
    var delegate:MovieGridViewModelDelegate?
    
    init(movieGrid: MovieGridModel, realm:RealmManager) {
        
        self.movieGridModel = movieGrid
        
        self.realm = realm
    }

     // - MARK: LOAD MOVIES
    
    func loadMovies(forceToServer:Bool=false){
        preconditionFailure("This method must be overridden")
    }
    
    // - MARK: LOAD NEXT PAGE
    
    func nextPage(increment:Bool = true)
    {
        // if is empty data
        
        if (self.movieGridModel.totalpages == 0) {
            
            self.loadMovies()
            
        } else {
            
            if(increment)
            {
                self.realm.nextPage(movieModel: self.movieGridModel)
            }
            
            if(self.movieGridModel.currentPage > self.movieGridModel.loadedPage)
            {
                self.loadMovies()
                
            }else{
                
                self.loadOfflineData()
            }
        }
        
    }
    
    // - MARK: LOAD OFFLINE DATA
    
    internal func loadOfflineData(){
        preconditionFailure("This method must be overridden")
        
    }
    
    // - MARK: FAVORITE MY MOVIE
    
    func selectedFavorite(movie:MovieVO){
        preconditionFailure("This method must be overridden")
    }

    
}
