//
//  MovieGrid.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 06/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import RealmSwift


class MovieGridModel: Object {
    
    dynamic var type = 99
    dynamic var currentPage = 1
    dynamic var loadedPage = 1
    dynamic var totalpages = 0
    dynamic var totalresults = 0
    
    var movies = List<MovieVO>()
    
    convenience init(type:Int)
    {
        self.init()
        
    }
    
    override static func primaryKey() -> String? {
        return "type"
    }
    
}
