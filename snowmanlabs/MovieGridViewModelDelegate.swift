//
//  MovieGridViewModelDelegate.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 08/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import RealmSwift

protocol MovieGridViewModelDelegate {
    func onLoadedMovies(movies:Object?)
}
