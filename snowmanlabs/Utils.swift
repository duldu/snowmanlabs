//
//  Utils.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import UIKit

class Utils
{
    static let sharedInstance: Utils = {
        let instance = Utils()
        // setup code
        return instance
    }()


    // MARK: VARIABLES
    
    //Loader
    var actInd: UIActivityIndicatorView?
    var container: UIView?
    var loadingView: UIView?

    // MARK: LOADER
    
    func showLoader()
    {
        let viewTop:UIView = self.getTopViewController().view
        
        container = UIView()
        container?.frame = viewTop.frame
        container?.center = viewTop.center
        container?.backgroundColor = UIColor.init(hexString: "0xffffff", alpha: 0.3)
        
        loadingView = UIView()
        loadingView?.frame = CGRect(x:0, y:0, width:80, height:80)
        loadingView?.center = container!.center
        loadingView?.backgroundColor = UIColor.init(hexString: "0x444444", alpha: 0.7)
        loadingView?.clipsToBounds = true
        loadingView?.layer.cornerRadius = 10
        
        actInd = UIActivityIndicatorView()
        actInd?.frame = CGRect(x:0, y: 0, width: 40, height:40);
        actInd?.center = CGPoint(x: loadingView!.frame.size.width/2, y: loadingView!.frame.size.height/2)
        actInd?.hidesWhenStopped = true
        actInd?.activityIndicatorViewStyle =
            UIActivityIndicatorViewStyle.whiteLarge
        
        
        loadingView?.addSubview(actInd!)
        container?.addSubview(loadingView!)
        viewTop.addSubview(container!)
        actInd?.startAnimating()
    }
    
    func hideLoader() {
        container?.removeFromSuperview()
        loadingView?.removeFromSuperview()
        actInd?.stopAnimating()
        actInd?.removeFromSuperview()
        
    }
    
    func getTopViewController() -> UIViewController {
        
        var viewController = UIViewController()
        
        if let vc =  UIApplication.shared.delegate?.window??.rootViewController {
            
            viewController = vc
            var presented = vc
            
            while let top = presented.presentedViewController {
                presented = top
                viewController = top
            }
        }
        
        return viewController
    }
    
    
}
