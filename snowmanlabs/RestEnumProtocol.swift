//
//  RestEnumProtocol.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import Alamofire

protocol RestEnumProtocol {
    
    var path : String{ get }
    var method : HTTPMethod { get }
    var parameters: [String:AnyObject]? { get }
    
}
