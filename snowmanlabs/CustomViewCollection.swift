//
//  CustomViewCollection.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 13/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import UIKit

class CustomViewCollection: UICollectionView
{
    var reloadDataCompletionBlock: (() -> Void)?
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if reloadDataCompletionBlock != nil {
            reloadDataCompletionBlock!()
        }
    }
    
    func reloadDataWithCompletion(completion:@escaping () -> Void) {
        reloadDataCompletionBlock = completion
        super.reloadData()
    }
}
