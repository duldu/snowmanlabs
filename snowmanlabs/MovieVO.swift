//
//  MovieVO.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 03/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import EVReflection
import RealmSwift

class MovieVO:Object, EVReflectable
{
    dynamic var poster_path: String!
    dynamic var adult = 0
    dynamic var overview:String!
    dynamic var release_date:String!
    dynamic var id = 0
    dynamic var original_title:String!
    dynamic var original_language:String!
    dynamic var title:String!
    dynamic var backdrop_path:String!
    dynamic var popularity = 0
    dynamic var vote_count = 0
    dynamic var video = 0
    dynamic var vote_average = 0
    dynamic var favorite = 0
    let genre_ids = List<IntObjectt>()
    
    
}

class IntObjectt: Object, EVReflectable{
    dynamic var value = 0
}
