//
//  MovieGridFavoriteViewModel.swift
//  snowmanlabs
//
//  Created by Eduardo de Araujo on 07/05/17.
//  Copyright © 2017 Eduardo de Araujo. All rights reserved.
//

import Foundation
import RealmSwift

class MovieGridFavoriteViewModel:MovieGridViewModelBase {
    
    
    
    // - MARK: LOAD MOVIES
    
    override func loadMovies(forceToServer:Bool=false)
    {
       self.loadOfflineData()
    }
    
    override func nextPage(increment:Bool = true)
    {
        self.loadOfflineData()
    }
    
    internal override func loadOfflineData()
    {
        let predicate = NSPredicate(format: "type = %d",  GridTypeEnum.Favorite.rawValue)
        
        if let movie = realm.getObjects(type: MovieGridModel.self)?.filter(predicate){
            print(" ===== offline  ===== : Favorite ")
            print(movie)
            // load model view from data
            if(movie.first != nil && self.movieGridModel.totalpages == 0 && self.movieGridModel.movies.count == 0)
            {
                let movieGridModel:MovieGridModel = movie.first as! MovieGridModel
                
                self.movieGridModel.currentPage = movieGridModel.currentPage
                self.movieGridModel.loadedPage = movieGridModel.loadedPage
                self.movieGridModel.totalpages = 1
                self.movieGridModel.totalresults = movieGridModel.totalresults
                self.movieGridModel.type = movieGridModel.type
                self.movieGridModel.movies = movieGridModel.movies
                
            }

            
            
            self.delegate?.onLoadedMovies(movies:movie.first)
            
        }
    
    }
    
    override func selectedFavorite(movie:MovieVO){
        
        if(movie.favorite == 1)
        {
            let listMovie = List<MovieVO>()
            
            //listMovie.append(objectsIn: self.movieGridModel.movies)
            listMovie.append(movie)
            
            
            self.realm.updateMovies(movieModel: self.movieGridModel, movies: listMovie)
            
        } else {
            
        }
        
    }
    
}
